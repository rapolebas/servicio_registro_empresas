from . import db
from .token import Token
import uuid
from datetime import datetime
from flask_security import UserMixin
from flask_mail import Mail, Message


roles_users = db.Table('roles_users',
        db.Column('user_id', db.Integer(), db.ForeignKey('user.id')),
        db.Column('role_id', db.Integer(), db.ForeignKey('role.id')))


# Create models
class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(100))
    last_name = db.Column(db.String(100))
    login = db.Column(db.String(80), unique=True)
    email = db.Column(db.String(120), unique=True)
    password = db.Column(db.String(200))
    date = db.Column(db.DateTime(), default=datetime.now())
    active = db.Column(db.Boolean, default=True)
    confirmed_at = db.Column(db.DateTime())

    roles = db.relationship('Role', secondary=roles_users,
                            backref=db.backref('users', lazy='dynamic'))
    transacciones = db.relationship('Transaccion', back_populates='usuario')
    tokens = db.relationship('Token', back_populates='usuario')

    def __str__(self):
        return "{}, {}, {}".format(self.login, self.last_name, self.first_name)

    def __repr__(self):
        return "{}: {}".format(self.id, self.__str__())

    @property
    def is_active(self):
        return self.active

    @property
    def is_anonymous(self):
        return False

    def get_id(self):
        return self.id

    # Required for administrative interface
    def __unicode__(self):
        return self.username

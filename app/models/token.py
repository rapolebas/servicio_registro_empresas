# -*- coding: utf-8 -*-
from . import db


class Token(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    name = db.Column(db.String(200), nullable=False)
    valor = db.Column(db.String(100))
    active = db.Column(db.Boolean, default=False)
    vencimiento = db.Column(db.DateTime)
    uso = db.Column(db.Integer, default=0)
    usos = db.Column(db.Integer, default=50)

    usuario_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    usuario = db.relationship('User', back_populates='tokens')

    def __str__(self):
        return "[{}] {}".format(self.name, self.valor)

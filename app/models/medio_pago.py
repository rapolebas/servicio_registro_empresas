from . import db


class MedioPago(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    name = db.Column(db.String(64), nullable=False)
    code = db.Column(db.String(64))
    token = db.Column(db.String(64))
    active = db.Column(db.Boolean, default=True)
    state = db.Column(db.String(10), default="test")
    fees = db.Column(db.Float, default=0.0)

    pasarela_id = db.Column(db.Integer, db.ForeignKey('pasarela_pago.id'))
    pasarela = db.relationship('PasarelaPago', back_populates='medios_pago')

    def __str__(self):
        return "{}".format(self.name)

from . import db
from app.models.provincia import Provincia


class Comuna(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    name = db.Column(db.String(64), nullable=False)
    code = db.Column(db.String(64))
    provincia_id = db.Column(db.Integer(), db.ForeignKey(Provincia.id))
    provincia = db.relationship(Provincia, foreign_keys=provincia_id, backref='comuna_info')
    active = db.Column(db.Boolean, default=True)

    def __str__(self):
        return "[{}] {}".format(self.code, self.name)

from . import db
from app.models.region import Region


class Provincia(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    name = db.Column(db.String(64), nullable=False)
    code = db.Column(db.String(64))
    region_id = db.Column(db.Integer(), db.ForeignKey(Region.id))
    region = db.relationship(Region, foreign_keys=region_id, backref='provincia_info')
    active = db.Column(db.Boolean)

    def __str__(self):
        return "[{}] {}".format(self.code, self.name)

from app.models import db
from app.models.ciudad import Ciudad, Provincia
from .admin import admin
from .my_model_view import MyModelView
import flask_login as login


class CiudadAdmin(MyModelView):

    def is_accessible(self):
        return login.current_user.is_authenticated and login.current_user.has_role('admin')


admin.add_view(CiudadAdmin(Ciudad, db.session, category="Other"))
admin.add_view(CiudadAdmin(Provincia, db.session, category="Other"))

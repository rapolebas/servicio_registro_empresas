# -*- coding: utf-8 -*-
from app.models import db
from app.models.token import Token
from .admin import admin
from .my_model_view import MyModelView
import flask_login as login


class TokenAdmin(MyModelView):

    def is_accessible(self):
        return login.current_user.is_authenticated and login.current_user.has_role('admin')


admin.add_view(TokenAdmin(Token, db.session, category="Other"))

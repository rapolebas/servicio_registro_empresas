from flask_admin.contrib import sqla
from flask_admin.base import MenuLink
import flask_login as login
from . import acteco
from . import archivo_empresas
from . import admin
from . import ciudad
from . import company_info
from . import company_info_remote
from . import medio_pago
from . import moneda
from . import pasarela_pago
from . import screen
from . import tag
from . import token
from . import tree
from . import user

admin.admin.add_sub_category(name="Links", parent_name="Other")
admin.admin.add_link(MenuLink(name='Back Home', url='/', category='Links'))
admin.admin.add_link(MenuLink(name='Google', url='http://www.google.com/', category='Links'))
admin.admin.add_link(MenuLink(name='Mozilla', url='http://mozilla.org/', category='Links'))


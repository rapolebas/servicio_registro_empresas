from flask import flash, render_template
from app.models import db
from app.models.archivo_empresas import ArchivoEmpresas
from app.models.company_info import CompanyInfo
from app.models.region import Region
from .admin import admin
from .my_model_view import MyModelView, FileUploadB64
from flask_admin.actions import action
from flask_admin.babel import gettext, lazy_gettext
from base64 import b64decode
from flask_admin import expose
import datetime
import csv
import re
import os
import flask_login as login


class FileView(MyModelView):

    def is_accessible(self):
        return login.current_user.is_authenticated and login.current_user.has_role('admin')

    # Override form field to use Flask-Admin FileUploadField
    column_list = ['name', 'date', 'state']
    form_create_rules = ('file_content', 'name')
    form_edit_rules = ('name', 'state', 'date')

    form_overrides = {
        'file_content': FileUploadB64
    }

    form_args = {
        'file_content': {
            'label': 'File',
            'column_name': 'name'
        }
    }

    def process_csv(self, s):
        s2 = re.sub('(?<=[0-9]{7}-[0-9K])(;)(?=\w)', ';|', s)
        arch = re.sub('(?<=[A-Z. ",)->¨+0-9;])(;)(?!;)(?=[0-9]*;)', '|;', s2)
        all_lines = arch.splitlines()
        datos = csv.reader(all_lines, csv.excel, delimiter=';', quotechar='|')
        i = 0
        total_datos = len(all_lines)
        ant_time = datetime.datetime.now()
        delta_time = 0
        time_left = datetime.datetime.now()
        per = 0
        for row in datos:
            per = round(((i*100)/total_datos), 2)
            delta_time = datetime.datetime.now() - ant_time
            time_left = datetime.datetime.now() + (delta_time *(total_datos-i))
            ant_time = datetime.datetime.now()
            print("%s %s  %s" %(str(i), str(per), time_left.strftime("%Y-%m-%d %H:%M:%S")))
            print(row)
            if i == 0:
                cabecera = [
                                'rut', 'name', 'numero_resol',
                                'fecha_resol', 'dte_email', 'url'
                           ]
                i += 1
                continue
            j = 0
            empresa = db.session.query(CompanyInfo).filter(CompanyInfo.rut==row[0]).first() or CompanyInfo()
            actualizar = False
            for key in cabecera:
                val = row[j]
                if key == 'region':
                    regiones = db.session.query(Region).filter(Region.prefijo==row[j]).all()
                    val = None
                if key in ['name']:
                    val = row[j].title()
                if key in ['fecha_resol', 'fecha_auth']:
                    val = datetime.datetime.strptime(row[j].replace(' ', ''), "%d-%m-%Y").date()
                if getattr(empresa, key) != val:
                    actualizar = True
                    setattr(empresa, key, val)
                j += 1
            if actualizar:
                empresa.actualizado = datetime.datetime.now()
            if not empresa.id:
                db.session.add(empresa)
            i += 1

    @action('process',
            lazy_gettext('Process File'),
            lazy_gettext('Are you sure you want to process selected records?'))
    def process_file(self, ids):
        try:
            for pk in ids:
                model = self.get_one(pk)
                s = b64decode(model.file_content).decode('iso-8859-1')
                self.process_csv(s)
                model.state = 'done'
            db.session.commit()
            flash('Record was successfully processed.', 'success')
        except Exception as ex:
            flash(gettext('Failed to process records. %(error)s', error=str(ex)), 'error')


    @expose('/process_local')
    def process_local(self):
        csvs = []
        print("yesr %s" % os.path.realpath(os.path.dirname(__file__)))
        for root, dirs, files in os.walk("%s/../data_imp" %os.path.realpath(os.path.dirname(__file__))):
            print("yesr %s" %dirs)
            csvs = files
        while csvs:
            arch = open('%s/../data_imp/%s' % (os.path.realpath(os.path.dirname(__file__)), csvs[0]), 'rb').read().decode('iso-8859-1')
            self.process_csv(arch)
            db.session.commit()
            del(csvs[0])

        return render_template('index.html')


admin.add_view(FileView(ArchivoEmpresas, db.session))

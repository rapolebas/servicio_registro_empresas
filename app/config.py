from app import app
from . import models

app.config['FLASK_ADMIN_SWATCH'] = 'cerulean'

# Create dummy secrey key so we can use sessions
app.config['SECRET_KEY'] = '123456790'

# Create in-memory database
app.config['DATABASE_FILE'] = 'sample_db.sqlite'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + app.config['DATABASE_FILE']
app.config['SQLALCHEMY_ECHO'] = False
app.config['MAIL_SERVER'] = 'smtp.example.com'
app.config['MAIL_PORT'] = 465
app.config['MAIL_USE_SSL'] = True
app.config['MAIL_USERNAME'] = 'username'
app.config['MAIL_PASSWORD'] = 'password'


class Config(object):
    JOBS = [
        {
            'id': 'reset_recs',
            'func': 'app.config:reset_recs',
            'args': (),
            'trigger': 'interval',
            'days': 1
        }
    ]

    SCHEDULER_API_ENABLED = True


def reset_recs():
    users = models.db.session.query(models.user.User).filter().all()
    for user in users:
        user.token_uso = 0
    models.db.session.commit()


app.config.from_object(Config())

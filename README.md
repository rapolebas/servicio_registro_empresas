Servicio de Registro de Empresas
------------

- Este programa básicamente se crea para generar una api pública para ser utilizado en distintos programas de facturación electrónica y tener un acceso actualizado de los datos de contribuyentes Chilenos, principlamente para fomentar el uso masivo del ERP Odoo y Flektra, distribuidos por la empresa y futura cooperativa https://odoocoop.cl/

- Se habilita la opción de:
- Recibir mediante PUT la actualización de 1-n registros.
- Sincronización total de la base de datos a modo de "Enjambre" con el servidor principal de este proyecto (por ahora https://sre.cl).
- El servicio contará con un token público "token_publico" el cual dará derecho a 200 consultas diarias.
- Podrán registrarse para obtener un token premium por el valor de CLP$1.000 +iva (si lo requieren con factura) anuales, que permitirá 50 consultas diarias.
- El dinero obtenido por token es un valor simbólico de "donación", para efectos de mantención y mejoras del sistema.

Información que entrega
------------
versión pública

-    Razón Social
-    RUT
-    DTE Email
-    Actecos *
-    Glosa de Giro *
-    Fecha Resolución
-    Dirección ( comuna, provincia, región )*

Para registrados ( todo lo anterior +)

-    Email *
-    Teléfono *
-    Logo*


* Información puede no estar disponible 


Installation
------------
First install virtualenv

To install Flask-Admin, simply::

    pip3 install flask-admin

1. Create and activate a virtual environment::

    git clone https://gitlab.com/dansanti/servicio_registro_empresas.git
    cd servicio_registro_empresas

2. Create and activate a virtual environment::

    virtualenv env -p python3
    source env/bin/activate

3. Install requirements::

    pip3 install -r 'requirements.txt'

4. Run the application::

    python3 ./run.py

Documentación : 
------------
    https://flask-restful.readthedocs.io/en/latest/
    http://flask-admin.org/


Archivo CSV del SII : 
------------
    
    https://drive.google.com/open?id=1GrQLy3nfOtCj7dZWfjRRY7gGQZvJFQY3

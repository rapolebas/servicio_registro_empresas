#!/usr/bin/env python3
import os
import os.path as op
import app


if __name__ == '__main__':
    # Build a sample db on the fly, if one does not exist yet.
    app_dir = "%s/app" % op.realpath(os.path.dirname(__file__))
    database_path = op.join(app_dir, app.app.config['DATABASE_FILE'])
    if not os.path.exists(database_path):
        app.build_sample_db.build_sample_db()

    # Start app
    app.app.run(host='0.0.0.0', debug=True)
